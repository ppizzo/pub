# CI/CD pipeline badges

![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-running-blue.png)
![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-interrupted-lightgrey.png)
![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-failed-red.png)
![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-test_failed-yellow.png)
![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-passed-brightgreen.png)

```markdown
![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-running-blue.png)
![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-interrupted-lightgrey.png)
![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-failed-red.png)
![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-test_failed-yellow.png)
![](https://gitlab.com/ppizzo/pub/raw/master/img/badges/pipeline-passed-brightgreen.png)
```
